# 1

# What Are Requirements? 

Title: What Are Requirements?
Keywords: Volere, Agile, Evolve, and Stakeholders

**First Paragraph:**

The main points of the article, "What Are Requirements", are the most useful products are those where the developers have understood what the product is intended to accomplish for its users, and how it must accomplish that purpose. No product has ever succeeded without prior understanding of it's requirements. The article explains to us how we know we found the correct requirments and how to communicate them. We go ever the process called "Volere". This is important because if done correctly, other people that pick up the project will be able to understand it. Requirements are usually expressed as an abstraction, so they do not influence the design of the solution. Product design allows us to translate the requirments into a plan to build something. 

Notes from the Book 

"It is equally important to note that a well-organized requirements process means that requirements, design, and implementation can be done as a number of iterative loops."

Requirements spec provides a complete description of the needed functionality and behavior of the product. 

System modeling produced working models of the functions and data needed by the product as well as models of the work to support the requirements process.

Product Design turns the abstract specification into a design suitable for the real world.

Once the product is built it immediately evolves. 

The only effective way of doing so is if you correctly understand what the product has to do, how it will be used, by whom it will be used, how it fits into the larger picture of the organization, and so on

The courts are jammed with lawsuits between software suppliers and clients where the source of disagreement is not whether the software works, but whether it does what is needed

Requirements need to be collaborative 

Change is cheap in the beginning- so encourage change in the beginning of the lifecycle 

Rabbit projects have short lives 

Horse projects are the most common corporate projects 

Elephants have the need to for a complete requirements spec 

A requirement is something the product must do or a quality it must have. A requirement exists either because the type of product demands certain functions or qualities or because the client wants that requirement to be part of the delivered product.

A functional requirement is an action that the product must take if it is to be useful to its users. Functional requirements arise from the work that your stakeholders need to do. Almost any action—calculate, inspect, publish, or most other active verbs—can be a functional requirement. -verbs

Nonfunctional requirements are properties, or qualities, that the product must have. In some cases, nonfunctional requirements—these describe such properties as look and feel, usability, security, and legal restrictions—are critical to the product’s success. Nonfunctional requirements are usually—but not always—determined after the product’s functionality. That is, once we know what the product is to do, we can determine requirements for how it is to behave, what qualities it is to have, and how fast, usable, readable, and secure it shall be.

Constraints are global requirements. They can be constraints on the project itself or restrictions on the eventual design of the product. - Product needs to be available at the beginning of the next tax year.. 

High level requirments --> (detailed functionality) and requirements --> non functional requirements && constraints 
Illustrated : Figure 1.3

Volere template is Appendix B 
Effectively it is a sophisticated checklist 

Browse through the template before you go too much further in this book. You will find a lot about writing requirements, plus much food for thought about the kinds of requirements to be gathered.

Requirements have a type they are located in the template 

Detailed model of the requirement process - https://learning.oreilly.com/library/view/mastering-the-requirements/0321419499/apa.html

**Second Paragraph:**

The strengths of the paper are explaining requirements, differentiating strengths of projects with animals, how systems modeling works, and product iteration. Additionally. the authors clearly lay out where all the resources are for the Volere template and the flash card for requirement stages. The weakness of the article is explaining "why requirements".. By the time I read this section I'm fully justified/convinved that I need them, and not questioning - too wordy.   

# A Labratory for Teaching Object Oriented Programming, @inproceedings{Beck1989ALF, title={A laboratory for teaching object oriented thinking}, author={Kent L. Beck and Ward Cunningham}, booktitle={OOPSLA '89},year={1989}}

Title: A labratory for Teaching Object Oriented Programming 
Keywords: Problem, Perspective, CRC Cards, Experience, and Object-ness

**First Paragraph:**

The main point of the article, "A Labratory For Teaching Object-Oriented Thinking", is introducing CRC cards for giving learners direct experience of objects and allowing for better design. Additionally, the best way to teach objects is to immerse the learner in "object-ness" of the material. The article goes deeper into the creation of CRC cards which allow us to represent multiple objects simultaneously and place the designers focus on the motivation for collaboration by representing many messages as a phrase of english text. Additionally, the article goes on to talk about the experience of others using CRC cards, outcomes from novices and experienced programmers. In conclusion, the value of objects is easier to get across with CRC cards - regardless of a novice or master programmer. 


**Second Paragraph:**

The strengths of this paper are explaining the benefits of CRC cards for giving readers experience with objects. Whether they be experienced or novice, the learner can grasp objects better if they have never done OOP, and if they're experienced it's easier to model/understand complex designs. The weakness is the explanation of the example taken from Smalltalk-80 image. We see the first card make a lot of sense with the definition of ClassName, but the following cards placed such as "view" and "controller" which are overlapping doesn't make much sense. I wish there was a step by step guide into the contstruction of the CRC Card and the translation to code!

# Personal Software Process (PSP) Script Humphrey, Watts. 2000. The Personal Software Process (PSP) (Technical Report CMU/SEI-2000-TR-022). Pittsburgh: Software Engineering Institute, Carnegie Mellon University. http://resources.sei.cmu.edu/library/asset-view.cfm?AssetID=5283

Title: Personal Software Process (PSP) Script 
Keywords: Problem, Perspective, CRC Cards, Experience, and Object-ness

**First Paragraph**

The PSP Script is a document that we are using to follow for the 99 Bottles and KWIC assignment. It breaks down 6 segments of Purpose - "Entry Criteria, Planning, Development, Acceptance Test, Evaluation, and Exit Criteria". Each segment has a guide, located on the right hand side of each segment, in the development of module level programs. 

**Second Paragraph**

The strengths of this document is that it has a nice outline and it is easy to follow. There are no weaknesses to be found on first glance. May add some as I use it during the completion of Bottles and KWIC. 

# Are We There Yet? Agile Situational Awareness Using Estimation by George Dinwiddie at DCSUG YouTube, YouTube, 25 Oct. 2020, https://www.youtube.com/watch?v=DyltKNFkZk8. Accessed 19 Jan. 2022. 

Title: Are We There Yet? Agile Situational Awareness Using Estimation by George Dinwiddie at DCSUG
Keywords: Agile, Situation, Awareness, Agile Estimation, 

**First Paragraph**

The video, Are We There Yet? Agile Situational Awareness Using Estimation by George Dinwiddie at DCSUG, is about finding your estimated travel time and what assumptions one made to get to those estimates - he starts out by relating this to traveling with family and then pivots to Agile Situational Awareness for software development projects. George goes on to explain that the trip estimates are not for the beginning and deciding the best time to leave, but to keep track along the way- leaving some extra time for some things that might happen. This relates to Situatuional Awareness in dynamic systems - which is the perception of the elements in the environment within a volume of time and space, the comprehension of thier meaning, and the projection of thier status in the near future. In software development projects, along the way some things can come up, if you're in a rush you may miss completely, and people are depending on you. You have to compare you estimates with your actuals, enjoy the project, and finish the plan.

**Second Paragraph**

The strenghts of this video is how well George Dinwiddie relates Agile Situational Awareness to going on a trip to meet a friend for dinner. A lot of things can go wrong, and you have to be able to anticipate change, and adjust accordingly. I believe overall this is a great way to explain a somewhat complex topic in simplistic terms. The weakness of the video was that it could have been shorter, there are a lot of gaps where he is expecting participation and does not get it. 

# #NoEstimates YouTube, YouTube, 5 July 2015, https://www.youtube.com/watch?v=QVBlnCTu9Ms. Accessed 23 Jan. 2022. 

Title: #NoEstimates
Keywords: Estimates, 80% of Projects Fail, Proof, Agile

**First Paragraph**

The video, #NoEstimates, is how we need to stop doing estimations now. Estimation has no value at all because they're always based on guess work.
The guesses are always wrong, but for some reason we always behave as if we didn't know that. We pretend that the estimates have some sort of validity to them but deep down we know that they don't. I couldn't agree more with this video and these statements, even if you are making financial estimate and you're using a model/training data sets.. There could always be a black swan - for instance COVID19. Estimates in terms of programming are useless because you're making a guess on something that hasn't been implemented before, so any kind of measurement of something that happened in the past is guess work - they're always wrong.
Allen moves on to talk about how 80% of projects fail with estimates driving this (we don't know how to estimate stuff), and we need to try and do projects without estimates because if time matters more than what the programs are actually doing we are focusing on the wrong thing. Agile came along and tries to be interpreted by someone's world view - agile is done wrong a lot. "Story points were invente to obfuscate duration so managers would not pressure the team over estimates.  

Velocity = ideal time / actual time | The organization puts thing in place that slows velocity down (forms, and other bureacratic stuff)

Points per spring are dumb 

Estimating software is like estimating physics - we are not estimating something that is not estimatable 

The ritual of estimation is OCD.. lol. 
Fear has to do with spending money (how much do I have to invest, based on the estimate?)

(Think in terms of projections not estimates)


**Second Paragraph**

The strengths of this video is how everyone relies on estimates even though they don't work, yet people still do this. Allen Holub does a great job at crushing the credibility of estimates and explaining why we shouldn't do them anymore - Allen even goes back to who created time estimates because he needs someone to blame. Estimations leads to disfunction, where you have terrible schedules and your quality of work is significantly reduced. I couldn't agree more with this video and I couldn't find any weaknesses with it. Liked, and shared. Additionally, a huge takeaway is instead of obsessing over estimations, we should be thinking in projections as in "let's build this for you, it will be done in 1 month,  you can look at it and at that point we can make a decision on whether you want us to continue or not. (reduce risk in an agile way) - If we are doing the wrong thing we have the option of fixing it really early - we will not go a year and then nothing is going to work.  

# Cal Newport on why you need to change your workflow, not your habits, when it comes to boosting productivity

Title: Cal Newport on why you need to change your workflow, not your habits, when it comes to boosting productivity
Keywords: Deep Work, Shallow Work, Workflow, Productivity 

**First Paragraph**

The podcast, Cal Newport on why you need to change your workflow, not your habits, when it comes to boosting productivity, is about solitudee deprivation and changing workflow which in turn boosts productivity. Solitude deprivation is when you kill every moment when you can potentially be alone with your thoughts by looking at a screen - in line, at dinner and someone goes to the bathroom. Cal Newport believes that we have replaced deep work with shallow work because of the concept of solitude deprivation. The podcast goes on to talk about how Cal Newport approaches his work ranging from different deep work routines, to how he builds solitude and boredom into his days.  

**Second Paragraph**

I loved this podcast. I think the strengths of the podcast were his explanation of deep work in terms of rituals built around writing and coding. Rituals can differ depending on the character of the cognitive effort. Cal used to solve proofs as a professor, but for writing his deep work was not being captured. The flow state and deep work are not the same thing, but they are not different. Deliberate practice is very different than the flow state, but it is also considered deep work. The biggest strenght of the podcast was differentiating between deep and shallow work, and that busy is not necessarily "good". For instance, doing mundane tasts that could be roped into shallow work is not what moves the needle, deep work does. I think the weakness of this podcast is how fast they go, I felt like I need to read "Digital Minimalism" or have some sort of background reading on Cal's work because I had to look up some of the terms they were using. 

- Deep work is what moves the needle, produces more value/revenue
- Let me keep track on how much deep work I am doing, and is what I'm doing valuable? 
- This is what deep work is, this is what shallow work is - what is my ratio for 40 hours a week? 
- Schedule deep work, find the ratio, ask your boss

# 2 

<https://gitlab.oit.duke.edu/mrc90/fintech512-assignment2>
<https://gitlab.oit.duke.edu/mrc90/fintech512-assignment2-kwic>

# 3 

# Times for tasks and additional notes

* What are Requirements + Write Up/Notes - 37 Minutes 
* A Labratory for Teaching Object Oriented Programming + Write Up/ Notes - 33 Minutes 
* Are we there yet + Write Up/ Notes - 1 hr 15 mins
* No Estimates -  1 hr 0 hr 
* Fintech Assignment 2 Bottles - 2 hr 0 min
* Fintech Assignment 2 -KWIC - 4 hr 0 min
* CRC Cards - 20 min 
* Term Project Reading - 30 min 
